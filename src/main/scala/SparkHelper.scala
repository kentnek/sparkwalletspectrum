import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods

/**
 * [Description]
 *
 * Created on Sep 17, 2015.
 *
 * @author Kent
 */

/**
 * git pull
 * sbt package
 * spark-submit --class "SparkHelper" --jars mongo-java-driver-3.0.2.jar,mongo-hadoop-core-1.4.0.jar target/scala-2.11/walletspectrum-1.0.jar
 */

object SparkHelper {

    private val sparkConf = new SparkConf()
      .setAppName("Weekly Email")
      .setMaster("local[*]")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer") // kryo is much faster
      .set("spark.io.compression.codec", "lzf")

    val sc = new SparkContext(sparkConf)

    def retrieveTable(collectionName: String, inputQuery: String = "", inputFields: String = "") = {
        val config = new Configuration()
        config.set("mongo.input.uri", s"mongodb://10.240.189.184:27017/moneylover.$collectionName")

        if (!inputQuery.isEmpty) config.set("mongo.input.query", inputQuery)
        if (!inputFields.isEmpty) config.set("mongo.input.fields", inputFields)
        //config.set("mongo.input.fields", "{account: 1, _id: 0}")

        sc.newAPIHadoopRDD(
            config,
            classOf[com.mongodb.hadoop.MongoInputFormat],
            classOf[Object],
            classOf[BSONObject]
        ).values // ignore the key id
    }

    private val currencyString = scala.io.Source.fromFile("currency.json").mkString

    implicit val formats = DefaultFormats

    case class Currency(i: Int, c: String)

    private val mCurrencyMap = (JsonMethods.parse(currencyString) \ "data").extract[List[Currency]].flatMap(Currency.unapply).toMap

    def toCurrencyCode(id: Int): String = mCurrencyMap.getOrElse(id, "")

}
