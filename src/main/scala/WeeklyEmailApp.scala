import java.text.SimpleDateFormat

import org.apache.spark.HashPartitioner
import org.apache.spark.rdd.RDD

/**
 * Data aggregator for weekly emails.
 *
 * Created on Sep 06, 2015.
 *
 * @author Kent
 */
/**
 * git pull
 * sbt clean package
 * spark-submit --class "WeeklyEmailApp" target/scala-2.10/walletspectrum-1.0.jar
 */
object WeeklyEmailApp {

    //region Utility Methods

    def samplePrintln(rdd: RDD[_], fraction: Double = 0.1) = {
        rdd.sample(withReplacement = false, fraction, System.currentTimeMillis())
          .foreach(println)
    }

    def collectPrintln: RDD[_] => Unit = _.collect().foreach(println)

    // convert BSON value to Double
    def toDouble: Object => Double = _.asInstanceOf[Number].doubleValue

    // convert type to multiplier
    //    case 1 => 1 // income
    //    case 2 => -1 // expense
    def toMultiplier: Object => Int = 3 - 2 * _.asInstanceOf[Int]

    //endregion

    def main(args: Array[String]) {

        println(s"Cores = ${SparkHelper.sc.defaultParallelism}. Preparing...")

        val partitionCount = SparkHelper.sc.defaultParallelism //* 3

        val t0 = System.nanoTime()

        val format = new SimpleDateFormat("dd-MM-yyyy")
        val startDate = format.parse("07-09-2015")
        val endDate = format.parse("13-09-2015")

        // Converts input userIds into an RDD
        val userIds = SparkHelper.sc.parallelize(List(
            //            "53cde0df844d70b134000001",
            //            "5482b41c91a4dafa5000002a",
            //            "547d1a4e124ff10a1a00000d",
            //            "53a24f0231d134c222000003",
            //            "54aacb7c3b2f001234000083",
            //            "5590b3f55660fa546f000002",
            //            "550fda47cb980f8c25000005",
            //            "55b1b2522797882240000008"

            "53ad8a032212606604002e22",
            "54cf2036002c5c857027914a"

        )).map(x => (x, x)) // (userId, userId)

        // (userId, email)
        val userEmail = SparkHelper.retrieveTable("users", "{email: {$ne: null}}", "{email: 1}")
          .map(u => (u.get("_id").toString, u.get("email").toString))
          .join(userIds) // (userId, (email, userId)
          .values
          .map({ case (email, user) => (user, email) })

        // (accountId, (name, email, currencyCode))
        val accountUser = SparkHelper.retrieveTable("accounts", "{isDelete: false}", "{owner: 1, name: 1, currency_id: 1}")
          // (userId, accInfo = (accountId, name, currencyCode))
          .map(t =>
            (
              t.get("owner").toString,
              (
                t.get("_id").toString,
                t.get("name").toString,
                SparkHelper.toCurrencyCode(t.get("currency_id").asInstanceOf[Int])
                )
              )
          )
          .join(userEmail) // (userId, (accInfo, email)) - find accounts belonging to users in userEmail
          .values // (accInfo, email)
          .map({ case ((acc, name, currencyCode), email) => (acc, (name, email, currencyCode)) })
          .partitionBy(new HashPartitioner(partitionCount))
          .persist()

        val accountList = accountUser.keys.map(a => (a, a))
          .partitionBy(new HashPartitioner(partitionCount))
          .persist()

        // (categoryId, -1)
        val expenseCategory = SparkHelper.retrieveTable(
            "categories",
            "{isDelete: false, account: {$ne: null}, type: 2, exclude_report: false}",
            "{account: 1}")
          .map(c => (c.get("account").toString, c.get("_id").toString)) // (account, cate id)
          .join(accountList) // (accountId, (cateId, accountId))
          .values // (cateId, accountId)
          .mapValues(x => -1)

        // (accountId, (1, signed amount))
        val transSignedAmount = SparkHelper.retrieveTable(
            "transactions",
            "{" +
              "isDelete: false, " +
              "account: {$ne: null}, " +
              s"displayDate: {$$gte: {$$date: ${startDate.getTime}}, $$lte: {$$date: ${endDate.getTime}}}" +
              "}",
            "{account: 1, amount: 1, category: 1}"
        )
          // (accId, (amount, cateId))
          .map(t => (t.get("account").toString, (toDouble(t.get("amount")), t.get("category").toString)))
          .join(accountList) // (accId, ((amount, cateId), accId)) // limit transactions
          .values
          .map({ case ((amount, cateId), accId) => (cateId, (amount, accId)) }) // (categoryId, (amount, accountId))
          // (cateId, ( (amount, accId), Option[sign] ) )
          .leftOuterJoin(expenseCategory) // join with category to find sign of amount
          .values
          .map({
            case ((amt, accId), None) => (accId, (1, amt))
            case ((amt, accId), Some(sign)) => (accId, (1, amt * -1))
        })

        // (accountId, (count, balance))
        val accountInfo = transSignedAmount.reduceByKey({
            case ((count1, amt1), (count2, amt2)) => (count1 + count2, amt1 + amt2)
        })

        // (email, [accounts])
        val userAggAccounts = accountUser.leftOuterJoin(accountInfo)
          .map({
            case (acc, ((name, email, currencyCode), None)) => (email, (name, None))
            case (acc, ((name, email, currencyCode), Some((count, balance)))) => (email, (name, (count, balance, currencyCode)))
        }) // (email, (account name, (count, balance, currencyCode))
          .groupByKey() // (email, [accounts])

        userAggAccounts.persist()

        println(s"\nData from ${startDate.toString} to ${endDate.toString}:\n")

        //TODO: avoid collect
        userAggAccounts.collect().foreach({ case (email, accounts) =>
            println(email)

            accounts.foreach({
                case (name, None) => println(s"    '$name': no transactions")
                case (name, (count: Int, balance: Double, currencyCode: String)) =>
                    println(s"    '$name': $count transactions, net spending = ${"%.1f" format balance} $currencyCode")
            })

            println()
        })

        val t1 = System.nanoTime()
        println("Elapsed time: " + "%.3f".format((t1 - t0) / 1000000000.0) + "s")
        //saveRDD.saveAsNewAPIHadoopFile("file:///bogus", classOf[Any], classOf[Any], classOf[com.mongodb.hadoop.MongoOutputFormat[Any, Any]], config)

    }
}

/*

> db.accounts.findOne()
{
        "__v" : 1,
        "_id" : "8894a69df3db4e878a969e661733a6bb",
        "createdAt" : ISODate("2014-06-09T10:49:07.678Z"),
        "currency_id" : 4,
        "exclude_total" : false,
        "icon" : "icon",
        "isDelete" : true,
        "lastEditBy" : ObjectId("537ec3a5803f47e467000005"),
        "listUser" : [
                ObjectId("537ec3a5803f47e467000005")
        ],
        "name" : "Tèo",
        "owner" : ObjectId("537ec3a5803f47e467000005"),
        "tokenDevice" : "zuosJBiqG3ERx2DTbmelRBRIFPNWInWbYgskZD65UNQ=",
        "updateAt" : ISODate("2014-07-08T08:43:40.082Z")
}

> db.transactions.findOne()
{
        "_id" : "3dd3d299109d4aa59adb1e1b83efef9a",
        "tokenDevice" : "miAo5q/IOg9k8sbNey/j6R6NHJ8coqsw6nZUfNlOaSk=",
        "note" : "",
        "account" : "8894a69df3db4e878a969e661733a6bb",
        "category" : "b90791cd9f9049e7bd53577e80aa7ed6",
        "amount" : 5000,
        "lastEditBy" : ObjectId("537ec3a5803f47e467000005"),
        "longtitude" : 0,
        "latitude" : 0,
        "address" : "",
        "displayDate" : ISODate("2014-06-09T00:00:00Z"),
        "remind" : 0,
        "exclude_report" : false,
        "isPublic" : false,
        "createdAt" : ISODate("2014-06-09T10:51:18.228Z"),
        "updateAt" : ISODate("2014-06-09T10:51:18.229Z"),
        "isDelete" : false,
        "campaign" : [ ],
        "with" : [ ],
        "__v" : 0
}

> db.categories.findOne()
{
        "__v" : 0,
        "_id" : "93a60de3c5b84504a7980c970058e8dc",
        "account" : "8894a69df3db4e878a969e661733a6bb",
        "createdAt" : ISODate("2014-06-09T10:49:10.854Z"),
        "icon" : "ic_category_salary",
        "isDelete" : true,
        "lastEditBy" : ObjectId("537ec3a5803f47e467000005"),
        "metadata" : "salary0",
        "name" : "Salary",
        "tokenDevice" : "zuosJBiqG3ERx2DTbmelRBRIFPNWInWbYgskZD65UNQ=",
        "type" : 1,
        "updateAt" : ISODate("2014-07-08T08:51:27.354Z")
}

> db.users.findOne()
{
        "__v" : 0,
        "_id" : ObjectId("5355dbe99576800d5f000002"),
        "acceptSync" : true,
        "createdDate" : ISODate("2014-04-22T03:03:05.023Z"),
        "email" : "admin@xinhvip.net",
        "hashed_password" : "7afd591c615352dd5563824a47cd1e1c",
        "iconResource" : [ ],
        "lastLogin" : ISODate("2014-07-22T07:33:50.873Z"),
        "lastSync" : ISODate("2014-07-22T07:33:56.877Z"),
        "limitDevice" : 3,
        "purchased" : true,
        "role" : "User",
        "salt" : "R3Hwy",
        "settings" : {
                "setting_amount_display" : {
                        "isShorten" : false,
                        "showCurrency" : false,
                        "negativeStyle" : 0,
                        "decimalVisibility" : false,
                        "decimalSeparator" : 0
                },
                "setting_lang" : "en-us",
                "setting_date" : {
                        "datetimeFormat" : "DD/MM/YYYY",
                        "firstDayOfMonth" : 1
                },
                "daily_alarm" : 20,
                "show_add_it_later" : false,
                "on_location" : true
        },
        "userSubscribe" : true,
        "verifyEmail" : false
}

 */
