name := "WalletSpectrum"

version := "1.0"

scalaVersion := "2.10.4"

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
    artifact.name + "-" + module.revision + "." + artifact.extension
}

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-core" % "1.4.1" % "provided",
    "org.mongodb.mongo-hadoop" % "mongo-hadoop-core" % "1.4.0",
    "org.mongodb" % "mongo-java-driver" % "3.0.2",
    "org.json4s" %% "json4s-jackson" % "3.2.11"
)